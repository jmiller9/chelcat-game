#!/usr/bin/python

from PIL import Image
import os, math, time
import json
max_frames_row = 10.0
frames = []
tile_width = 64
tile_height = 64

spritesheet_width = 512
spritesheet_height = 512
imagedir = "images/"

files = os.listdir(imagedir)
files.sort()
print(files)
indices = {}

idx = 0
for current_file in files :
    try:
        with Image.open(imagedir + current_file) as im :
            frames.append(im.getdata())
            ssidx = current_file.rfind('.')
            img_name = current_file[:ssidx]
            indices[img_name] = idx
            idx += 1
    except:
        print(current_file + " is not a valid image")

tile_width = frames[0].size[0]
tile_height = frames[0].size[1]

if len(frames) > max_frames_row :
    spritesheet_width = tile_width * max_frames_row
    required_rows = math.ceil(len(frames)/max_frames_row)
    spritesheet_height = tile_height * required_rows
else:
    spritesheet_width = tile_width*len(frames)
    spritesheet_height = tile_height
    
print(spritesheet_height)
print(spritesheet_width)

spritesheet = Image.new("RGBA",(int(spritesheet_width), int(spritesheet_height)))

for current_frame in frames :
    top = tile_height * math.floor((frames.index(current_frame))/max_frames_row)
    left = tile_width * (frames.index(current_frame) % max_frames_row)
    bottom = top + tile_height
    right = left + tile_width
    
    box = (left,top,right,bottom)
    box = [int(i) for i in box]
    cut_frame = current_frame.crop((0,0,tile_width,tile_height))
    
    spritesheet.paste(cut_frame, box)
    
#spritesheet.save("spritesheet" + time.strftime("%Y-%m-%dT%H-%M-%S") + ".png", "PNG")
spritesheet.save("sprites/spritesheet.png", "PNG")

f = open('sprites/spritesheet.json', 'w')
try:
    f.write(json.dumps(indices))
except:
    print("Error saving spritesheet.json")
finally:
    f.close()