#!/usr/bin/python

import xmltodict

# Usage:
# m = Map('../maps/test.tmx')
# tiles = m.getTiles()
class Map(object):
    def __init__(self, filename):
        #TODO: This could be more robust and handle errors
        f = open(filename, 'r')
        xmldata = f.read()
        f.close()
        self.raw_data = xmltodict.parse(xmldata)
        self.width = int(self.raw_data['map']['@width'])
        self.height = int(self.raw_data['map']['@height'])
        self._parseTiles()
    
    #Right now this will only grab one layer, but that should be all we need
    def _parseTiles(self):
        self.tiles = []
        csv_data = self.raw_data['map']['layer']['data']['#text'].split('\n')
        for line in csv_data:
            indices = line.split(',')
            row = []
            for idx in range(0, self.width):
                try:
                    #We subtract 1 here to get the actual sprite index
                    row.append(int(indices[idx])-1)
                except:
                    #TODO: Throw an exception? This would cause the map to be broken on load...
                    print('Encountered an error parsing tiles')
            self.tiles.append(row)

    def getTiles(self):
        return self.tiles