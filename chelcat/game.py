#!/usr/bin/python

import pygame
import random
from sprites import SpriteSheet
from levelmap import Map
from pygame.locals import *

from player import Player
import os
my_dir = os.path.dirname(os.path.realpath(__file__))
parent_dir = my_dir[:my_dir.rfind('/')]

def getMinY(y, cy):
    if cy > 320:
        if y - 11 < 0:
            return 0
        else:
            return y - 11
    else:
        if y - 7 < 0:
            return 0
        else:
            return y - 7

def getMinX(x, cx):
    if cx > 448:
        if x - 15 < 0:
            return 0
        else:
            return x - 15
    else:
        if x - 9 < 0:
            return 0
        else:
            return x - 9

def getMaxX(x, max, cx):
    if cx < 448:
        if x + 16 > max:
            return max
        else:
            return x + 16
    else:
        if x + 10 > max:
            return max
        else:
            return x + 10

def getMaxY(y, max, cy):
    if cy < 320:
        if y + 12 > max:
            return max
        else:
            return y + 12
    else:
        if y + 7 > max:
            return max
        else:
            return y + 7

'''
TODO: The code below is a bit sloppy and should be in its own function...
'''
# initialize pygame
pygame.init()

pygame.display.set_caption("Chelcat's Adventure")

# create the screen object
# here we pass it a size of 1024x768
screen = pygame.display.set_mode((1024, 768))

# Create the spritesheet
spritesheet = SpriteSheet(parent_dir + '/sprites/spritesheet')

# Create a custom event for adding a new enemy.
ADDENEMY = pygame.USEREVENT + 1
#pygame.time.set_timer(ADDENEMY, 250)

# create our 'player'; right now he's just a rectangle
player = Player(spritesheet.get_cached_image('cat_right'))

background = pygame.Surface(screen.get_size())
background.fill((0, 175, 255))

#enemies = pygame.sprite.Group()
all_sprites = pygame.sprite.Group()
all_sprites.add(player)

running = True

#TODO: Just for testing, this is hard-coded
currentMap = Map(parent_dir + '/maps/test.tmx')
tiles = currentMap.getTiles()

frame = 0
maxX = currentMap.width * 64
maxY = currentMap.height * 64

# Main game loop
while running:
    playerX = player.x
    playerY = player.y
    playerTileX = player.getTileX()
    playerTileY = player.getTileY()

    centerX = 448
    centerY = 320

    if playerX - 448 < 0:
        centerX += (playerX - 448)
    elif playerX + 576 > maxX:
        if maxX >= 1024:
            centerX += (576 - (maxX - playerX))

    if playerY - 320 < 0:
        centerY += (playerY - 320)    
    elif playerY + 448 > maxY:
        if maxY >= 768:
            centerY += (448 - (maxY - playerY))

    for event in pygame.event.get():
        if event.type == KEYDOWN:
            if event.key == K_ESCAPE:
                running = False
        elif event.type == QUIT:
            running = False
        elif(event.type == ADDENEMY):
            pass
            '''
            new_enemy = Enemy()
            enemies.add(new_enemy)
            all_sprites.add(new_enemy)
            '''
    screen.blit(background, (0, 0))
    pressed_keys = pygame.key.get_pressed()
    player.update(pressed_keys, tiles)
    #enemies.update()

    # Draw the map
    startj = getMinY(playerTileY, centerY)
    endj = getMaxY(playerTileY, currentMap.height, centerY)
    for j in range(startj, endj):
        starti = getMinX(playerTileX, centerX)
        endi = getMaxX(playerTileX, currentMap.width, centerX)
        for i in range(starti, endi):
            tile = tiles[j][i]
            if (tile >= 0):
                x = (i * 64) - playerX + centerX
                y = (j * 64) - playerY + centerY
                screen.blit(spritesheet.get_cached_image_by_index(tile), (x, y))

    # Draw all other sprites
    for entity in all_sprites:
        entity.applyGravity(tiles)

        x = entity.x - playerX + centerX
        y = entity.y - playerY + centerY
        #screen.blit(entity.image, entity.rect)
        screen.blit(entity.image, (x, y))

    '''
    if pygame.sprite.spritecollideany(player, enemies):
        player.kill()
    '''

    pygame.display.flip()
