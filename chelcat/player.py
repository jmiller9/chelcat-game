#!/usr/bin/python
import pygame
from pygame.locals import *
from game_object import GameObject

class Player(GameObject):
    def __init__(self, image):
        super(Player, self).__init__(image)
        self.jump_distance = 0
        self.max_jump = 192

    # Handle keypresses
    def update(self, pressed_keys, tiles):
        if pressed_keys[K_UP] or pressed_keys[K_w]:
            if self.grounded:
                self.jump_distance = 0
            if self.jump_distance < self.max_jump:
                if self.moveVertical(tiles, -2 * self.gravity_multiplier):
                    self.grounded = False
                    self.jump_distance += (self.gravity_multiplier)
        if pressed_keys[K_LEFT] or pressed_keys[K_a]:
            self.moveHorizontal(tiles, -4)
        if pressed_keys[K_RIGHT] or pressed_keys[K_d]:
            self.moveHorizontal(tiles, 4)
        if pressed_keys[K_SPACE]:
            #TODO: fire a projectile
            pass