#!/usr/bin/python
import pygame
from pygame.locals import *

class GameObject(pygame.sprite.Sprite):
    def __init__(self, image):
        super(GameObject, self).__init__()
        self.image = image
        self.image.set_colorkey((255, 255, 255), RLEACCEL)
        self.rect = self.image.get_rect()
        self.leeway = (self.rect.width / 2) - 1
        self.gravity_multiplier = 4
        self.grounded = False
        self.x = self.rect.x
        self.y = self.rect.y
    
    def getTileX(self):
        return (self.x + self.leeway) / self.rect.width
    
    def getPredictedTileX(self, delta):
        return (self.x + self.leeway + delta) / self.rect.width
    
    def getTileY(self):
        return (self.y + self.leeway) / self.rect.height
    
    def getPredictedTileY(self, delta):
        return (self.y + self.leeway + delta) / self.rect.height
    
    def getTileYTop(self):
        return (self.y + self.rect.height) / self.rect.height
    
    def getTileYTopHorizontal(self):
        return (self.y + self.rect.height - 1) / self.rect.height
    
    def getTileYBottom(self):
        return self.y / self.rect.height
    
    def applyGravity(self, tiles):
        if tiles[self.getTileYTop()][self.getTileX()] < 0:
            self.y += self.gravity_multiplier
        else:
            self.grounded = True
    
    def moveHorizontal(self, tiles, delta):
        px = self.getPredictedTileX(delta)
        if len(tiles[self.getTileYTopHorizontal()]) > px and px >= 0:
            if tiles[self.getTileYTopHorizontal()][px] < 0:
                self.x += delta
    
    def moveVertical(self, tiles, delta):
        py = self.getPredictedTileY(delta)
        if py >= 0 and len(tiles) > py:
            if tiles[py][self.getTileX()] < 0:
                self.y += delta
                return True
        return False