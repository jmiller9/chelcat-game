#!/usr/bin/python

import pygame
import json

# This abstracts spritesheet indices away from the developer
# Instead of dealing with u,v coordinates, the dev just specifies
# An image name to load, assuming that image is in the spritesheet

# Usage:
# sheet = SpriteSheet("../sprites/spritesheet")
# sprite = sheet.get_image("cat_left")
class SpriteSheet(object):
    def __init__(self, name, width_height_sprite=64, width_height_sheet=512):
        self.width = int(width_height_sprite)
        self.height = int(width_height_sprite)
        self.rows = int(width_height_sheet / self.width)
        self.columns = int(width_height_sheet / self.height)
        self.name = name
        filepath = name + '.png'
        self.sheet = pygame.image.load(filepath).convert_alpha()
        self.indices = {}
        self.load_indices()
        self.cacheImages()
    
    def load_indices(self):
        json_filepath = self.name + '.json'
        try:
            f = open(json_filepath, 'r')
            self.indices = json.loads(f.read())
        except:
            print('Error reading ' + json_filepath)
        finally:
            f.close()
    
    def get_image_list(self):
        return self.indices.keys()
    
    # This gets a single sprite from the spritesheet
    def get_image(self, image_name):
        if image_name in self.indices:
            index = self.indices[image_name]
            x = (index % self.rows) * self.width
            y = int(index / self.columns) * self.height
            return self.sheet.subsurface((x, y, self.width, self.height))
        else:
            return None

    # This also gets a single sprite from the spritesheet
    def get_image_by_index(self, index):
        x = (index % self.rows) * self.width
        y = int(index / self.columns) * self.height
        return self.sheet.subsurface((x, y, self.width, self.height))
    
    # Buffers the subsurfaces so we aren't making new ones every frame
    def cacheImages(self):
        self.cachedImages = {}
        for key in self.indices:
            self.cachedImages[self.indices[key]] = self.get_image_by_index(self.indices[key])
    
    # A better version of get_image
    def get_cached_image(self, name):
        return self.cachedImages[self.indices[name]]
    
    # A better version of get_image_by_index
    def get_cached_image_by_index(self, index):
        return self.cachedImages[index]