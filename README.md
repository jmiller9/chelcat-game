This is the Chelcat Game. It is written in Python using Pygame.

Instructions for downloading a good IDE, Visual Studio Code, can be obtained at
https://code.headmelted.com/

For good image editing software (GIMP):
sudo apt-get install gimp

To make our maps, we are going to use Tiled.
sudo apt-get install tiled

Sprites are 64x64 pixels in size. When creating new maps, specify the height to be at least 12 tiles.
The width should be >= 16 tiles. We will render this game in a 1024x768 pixel window. The tile layer format
is CSV, and the render order is "Right Down".
