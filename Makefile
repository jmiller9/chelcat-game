run:
	python chelcat/game.py

.PHONY: sprites
sprites:
	python tools/gen_spritesheet.py

clean:
	rm -f sprites/*